﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeElapse : MonoBehaviour
{

    private float time = 0.0f;
    public float interpolationPeriod = 2f;
    public GameObject objectPicture = null;
    public GameObject objectPicture2 = null;


    void Update()
    {
        time += Time.deltaTime;

        if (time >= interpolationPeriod)
        {
            time = 0.0f;

            if (objectPicture2)
            {
                objectPicture2.SetActive(!objectPicture2.activeSelf);
                objectPicture.SetActive(!objectPicture.activeSelf);
            }
            Destroy(objectPicture);
            if (!objectPicture2)
            {
                SceneManager.LoadScene(1);
            }
            // execute block of code here
        }
    }
}
